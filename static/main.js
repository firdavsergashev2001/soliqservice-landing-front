jQuery(document).ready(function ($) {
  // News slider
  const newsSwiper = new Swiper(".news-swiper", {
    loop: false,
    slidesPerView: 1,
    direction: "horizontal",
    speed: 1000,
    navigation: {
      nextEl: ".news-next",
      prevEl: ".news-prev",
    },
    breakpoints: {
      420: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
      576: {
        slidesPerView: 3,
        spaceBetween: 32,
      },
    },
  });
  // Text image slider
  const textImageSwiper = new Swiper(".text-img-swiper", {
    loop: false,
    slidesPerView: 1,
    direction: "horizontal",
    speed: 1000,
  });
  // Image Text slider
  const imageTextSwiper = new Swiper(".img-text-swiper", {
    loop: false,
    slidesPerView: 1,
    direction: "horizontal",
    speed: 1500,
    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    scrollbar: {
      el: ".img-text-scrollbar",
      hide: false,
    },
  });
  if (textImageSwiper && imageTextSwiper) {
    textImageSwiper.controller.control = imageTextSwiper;
    imageTextSwiper.controller.control = textImageSwiper;
  }
  // Materials slider
  const materialsSwiper = new Swiper(".materials-slider", {
    loop: false,
    slidesPerView: 1,
    direction: "horizontal",
    speed: 1000,
    navigation: {
      nextEl: ".materials-next",
      prevEl: ".materials-prev",
    },
    breakpoints: {
      420: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
      576: {
        slidesPerView: 3,
      },
      768: {
        slidesPerView: 4,
        spaceBetween: 32,
      },
    },
  });
  // Match height
  jQuery(".instructions-card__title").matchHeight({ property: "min-height" });
  // FAQS Accordion
  jQuery(".faq-accordion-item__header").on("click", function () {
    jQuery(this).next().slideToggle();
    jQuery(this).find(".faq-accordion-item__icon").toggleClass("active");
  });
});
