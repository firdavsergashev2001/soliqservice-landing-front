export class ApiService {
    static baseUrl = process.env.REACT_APP_BACKEND_URL || 'http://localhost:1337'
    static accessToken = process.env.REACT_APP_API_TOKEN || null;

    static async getComponent(componentName) {
        try{
            const response = await fetch(`${ApiService.baseUrl}/api/${componentName}`, {
                method: 'GET',
                headers: {
                    "Authorization": "Bearer " + ApiService.accessToken
                }
            });
            return await response.json();
        } catch(err) {
            return err;
        }
    }

    static async getArticle(id, locale = 'ru') {
        try{
            const response = await fetch(`${ApiService.baseUrl}/api/news/${id}?locale=${locale}&populate=*`, {
                method: 'GET',
                headers: {
                    "Authorization": "Bearer " + ApiService.accessToken
                }
            });
            const result = await response.json()
            return result;
        } catch(err) {
            return err;
        }
    }
}