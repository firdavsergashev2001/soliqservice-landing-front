export const systemLangs = {
    ru: {
        label: 'РУ',
        idx: 0,
        localization: 'ru'
    },
    uz: {
        label: 'UZ',
        idx: 1,
        localization: 'uz'
    },
    en: {
        label: 'EN',
        idx: 2,
        localization: 'en'
    }
}