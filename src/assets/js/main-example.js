jQuery(document).ready(function ($) {
  // Hero slider
  const heroSwiper = new Swiper(".hero-slider", {
    loop: false,
    slidesPerView: 1,
    direction: "horizontal",
    speed: 1000,
    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    navigation: {
      nextEl: ".hero-next",
      prevEl: ".hero-prev",
    },

    // mousewheel: {
    //   invert: true,
    // },
  });
  // Gallery slider
  const gallerySwiper = new Swiper(".gallery-slider", {
    loop: false,
    slidesPerView: 1,
    direction: "horizontal",

    speed: 1000,
    navigation: {
      nextEl: ".gallery-next",
      prevEl: ".gallery-prev",
    },
    breakpoints: {
      420: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
      576: {
        slidesPerView: 3,
        spaceBetween: 24,
      },
      768: {
        slidesPerView: 4,
        spaceBetween: 24,
      },
    },
  });

  // Burger
  const headerMenu = document.querySelector(".header__content");
  const burgerBtn = document.querySelector(".burger");
  const closeBtn = document.querySelector(".header-close");

  burgerBtn.addEventListener("click", function () {
    headerMenu.classList.add("active");
  });
  closeBtn.addEventListener("click", function () {
    headerMenu.classList.remove("active");
  });
  // Mobile submenu
  const subMenuIcon = jQuery(".sub-menu-icon");
  subMenuIcon.on("click", function () {
    jQuery(this).parents("li").find("ul").toggleClass("active");
  });

  // Year
  const yearSpan = document.getElementById("year");
  yearSpan.innerText = new Date().getFullYear();

  // Custom select
  jQuery("select").selectric();
  // Match height
  jQuery(".career-card__text").matchHeight({ property: "min-height" });

  // Parallax
  var s = skrollr.init({
    smoothScrolling: true,
  });
  if (s.isMobile()) {
    s.destroy();
  }
});
