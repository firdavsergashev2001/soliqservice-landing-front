export const systemLocales = {
    general: {
        email: "taxfree@soliq.uz",
        phone: "+998 71 202 32 12"
    },
    header: {
        taxfree: ["Tax Free", "Tax Free", "Tax Free"],
        news: ["Новости", "Yangiliklar", "News"],
        instructions: ["Инструкция", "Qo'llanma", "Instruction"],
        download: ["Скачать", "Yuklab olish", "Download"],
        faq: ["FAQ", "FAQ", "FAQ"],
        contacts: ["Контакты", "Kontaktlar", "Contacts"]
    },
    hero: {
        title: [
            "<span>SOLIQ</span>удобное мобильное приложение для возврата суммы НДС",
            "<span>SOLIQ</span>QQSni qaytarish uchun qulay mobil ilova",
            "<span>SOLIQ</span>convinient mobile app for VAT refund"
        ],
        comment: [
            "Soliq – одно из самых скачиваемых приложений в Узбекистане по версии App Store и Google Play.",
            "Soliq – App Store va Google Play maʼlumotlariga koʻra Oʻzbekistonda eng koʻp yuklab olinadigan ilovalardan biri hisoblanadi.",
            "Soliq – one of the most downloaded applications in Uzbekistan according to the App Store and Google Play.",
        ]
    },
    about: {
        title: [
            "Tax Free в Узбекистане",
            "Tax Free O'zbekistonda",
            "Tax Free in Uzbekistan",
        ],
        subtitle: [
            "Коротко о возврате суммы НДС",
            "QQSni qaytarish haqida qisqacha",
            "Briefly about the VAT refund"
        ],
        describtion: [
            "Tax Free - система возврата суммы налога на добавленную стоимость (НДС). Cумма НДС возвращается на покупки товаров, совершенные иностранными гражданами на территории Республики.",
            "Tax Free - qo'shilgan qiymat solig'ini (QQS) qaytarish tizimi. Xorijiy fuqarolar tomonidan respublika hududida sotib olingan tovarlar uchun QQS summasi qaytariladi.",
            "Tax Free - a system for the return of value added tax (VAT). The amount of VAT is refunded on purchases of goods made by foreign citizens on the territory of the Republic."
        ],
        additional: [
            `Возврат суммы НДС можно оформить в международных аэропортах <strong>Ташкента, Самарканда, Бухары, Ферганы и Ургенча.</strong>`,
            `QQSni <strong>Toshkent, Samarqand, Buxoro, Fargʻona va Urganch</strong> xalqaro aeroportlarida qaytarishingiz mumkin`,
            `VAT refunds can be issued at international airports <strong>Tashkent, Samarkand, Bukhara, Fergana and Urgench.</strong>`
        ]
    },
    news: {
        title: ["Новости", "Yangiliklar", "News"],
        subtitle: [
            "Все актуальные новости и события о Tax Free в Узбекистане", 
            "O'zbekistondagi Tax Free haqidagi barcha dolzarb yangiliklar", 
            "All current news and events about Tax Free in Uzbekistan"
        ],
        readAll: [
            "Читать полностью", "Batafsil", "More"
        ]
    },
    //newsSlider: {},
    instructions: {
        title: ["Инструкции", "Qo'llanma", "Instructions"],
        subtitle: ["Как получить возврат суммы НДС?", "QQSni qanday qaytarish mumkin?", "How to get a VAT refund?"],
        steps: [
            { 
                title: ["Установите мобильное приложение Soliq", "Soliq mobil ilovasini oʻrnating", "Install the Soliq mobile app"],
                content: [
                    `<li class="instructions-card__item">пройдите регистрацию с помощью Вашего номера мобильного телефона</li><li class="instructions-card__item">добавьте Вашу карту международной платежной системы</li>`,
                    `<li class="instructions-card__item">mobil telefon raqamingiz bilan ro‘yxatdan o‘ting</li><li class="instructions-card__item">xalqaro to‘lov tizimi kartangizni qo‘shing</li>`,
                    `<li class="instructions-card__item">register with your mobile phone number</li><li class="instructions-card__item">add your international payment system card</li>`
                ]
            },
            { 
                title: ["Сканируйте чеки", "Kvitansiyalarni skanerlang", "Scan receipts"],
                content: [
                    `<li class="instructions-card__item">в разделе «Общие услуги» выберите «Tax Free»</li><li class="instructions-card__item">сканируйте чеки с QR-кодом за приобретенные товары</li><li class="instructions-card__item">сумма НДС для возврата рассчитывается автоматически</li>`,
                    `<li class="instructions-card__item">"Umumiy xizmatlar" ostida "Tax Free"-ni tanlang</li><li class="instructions-card__item">xarid qilingan mahsulotlar uchun QR kod kvitansiyalarini skanerlang</li><li class= "instructions-card__item">QQSni qaytarish summasi avtomatik tarzda hisoblanadi</li>`,
                    `<li class="instructions-card__item">under "General Services", select "Tax Free"</li><li class="instructions-card__item">scan QR code receipts for purchased items</li><li class="instructions-card__item">VAT refund amount is calculated automatically</li>`
                ]
            },
            { 
                title: ["Посетите один из пунктов обслуживания", "Xizmat ko'rsatish punktlaridan biriga tashrif buyuring", "Visit one of the service points"],
                content: [
                    `<li class="instructions-card__item">При выезде из Республики Узбекистан обратитесь в один из пунктов обслуживания в международных аэропортах:</li><li class="instructions-card__item">для идентификации Вашей личности</li><li class="instructions-card__item">для возврата уплаченной суммы НДС</li>`,
                    `<li class="instructions-card__item">O‘zbekiston Respublikasidan chiqib ketayotganda, xalqaro aeroportlardagi xizmat ko‘rsatish shoxobchalaridan biriga murojaat qiling va: <li class="instructions-card__item">shaxsingizni tasdiqlang</li><li class="instructions-card__item">hisoblangan QQS summasini qaytarish uchun so'rov qoldiring</li>`,
                    `<li class="instructions-card__item">When leaving the Republic of Uzbekistan, contact one of the service points at international airports:</li><li class="instructions-card__item">to identify your person</li><li class="instructions-card__item">for VAT refund</li>`
                ]
            }
        ]
    },
    attachments: {
        title: ["Материалы", "Qo'shimcha ma'lumot", "Additional resources"],
        subtitle: ["Нажмите на иконку, чтобы начать скачивание необходимого файла", "Kerakli faylni yuklab olish uchun uning belgisini bosing", "Click on the icon to start downloading the required file"],
        button: ["Перейти ко всем файлам", "Barcha fayllarga o'tish", "Go to all files"]
    },
    faq: {
        title: [
            "Часто задаваемые вопросы",
            "Ko'p so'raladigan savollar",
            "Frequently Asked Questions"
        ],
        subtitle: [
            "ТОП вопросов и ответов по системе Tax Free в Узбекистане",
            "O'zbekistondagi Tax Free tizimi bo'yicha TOP savollar va javoblar",
            "TOP questions and answers on the Tax Free system in Uzbekistan"
        ],
        formTitle: [
            "Задайте свой вопрос",
            "Savolingizni bizga jo'nating",
            "Ask your question"
        ],
        formSubtitle: [
            "Если вы не нашли ответ на интересующий вас вопрос, то напишите его нам, предварительно заполнив все формы",
            "Agar siz savolingizga javob topa olmagan bo'lsangiz, unda barcha shakllarni to'ldirib, bizga jo'nating",
            "If you did not find the answer to your question, then write it to us by filling out all the forms beforehand"
        ],
        namePlaceholder: [
            "Ваше полное имя", "To'liq ismingiz", "Your full name"
        ],
        emailPlaceholder: [
            "Ваш E-mail", "Sizning E-mailingiz", "Your E-Mail"
        ],
        messagePlaceholder: ["Сообщение", "Savolingiz", "Message"],
        sendButton: ["Отправить", "Yuborish", "Send"]
    },
    map: {
        title: ["Контакты", "Kontaktlar", "Contacts"],
        subtitle: [
            "Выберите подходящий пункт обслуживания для возврата суммы НДС", 
            "QQSni qaytarish uchun tegishli xizmat ko'rsatish nuqtasini tanlang", 
            "Select the appropriate service point for the VAT refund"
        ]
    },
    branches: {
        subtitle: [
            "Филиалы пунктов обслуживания по возврату суммы НДС (Tax Free) в Узбекистане",
            "O‘zbekistonda QQSni qaytarish bo‘yicha xizmat ko‘rsatish shoxobchalari (Tax Free) filiallari",
            "Branches of service points for the return of VAT (Tax Free) in Uzbekistan"
        ]
    },
    contacts: {
        officeLabel: ["Головной офис", "Bosh idora", "Head office"],
        office: [`ГУП “Налог-сервис”`, `"Soliq-servis" DUK`, `SUE "Nalog-service"`],
        addressLabel: ["Адрес", "Manzil", "Address"],
        address: ["г. Ташкент, Чиланзарский район, улица Мукими 166", "Toshkent shahri, Chilonzor tumani, Muqimiy ko‘chasi, 166-uy", "Tashkent city, Chilanzar district, Mukimi street 166"],
        callCenterLabel: ["Call-центр", "Aloqa markazi", "Call center"],
        emailLabel: ["E-mail", "E-mail", "E-mail"]
    },
    footer: {
        legend: [
            `Электронные налоговые услуги: портал электронных государственных услуг налоговых органов.`,
            `Elektron soliq xizmatlari: soliq organlarining elektron davlat xizmatlari portali.`,
            `Electronic tax services: portal of electronic public services of tax authorities.`
        ],
        copyright: [
            `© 2022 Государственный налоговый комитет.`,
            `© 2022 Davlat soliq qo'mitasi.`,
            `© 2022 State Tax Committee.`
        ]
    },
    404: {
        title: ["Страница не найдена", "Siz qidirgan bet topilmadi", "Page not found"],
        link: ["Вернуться назад", "Ortga qaytish", "Go to home"]
    },
    notifications: {
        feedback: {
            invalidField: ["Недопустимое значение для поля", "Maydon uchun yaroqsiz mazmun kiritildi", "Invalid field value"],
            success: {
                title: ["Спасибо!", "Raxmat!", "Thank you!"],
                message: ["Ваше обращение отправлено", "Sizning so'rovingiz yuborildi", "Your request has been sent"]
            }
        }
    }
}