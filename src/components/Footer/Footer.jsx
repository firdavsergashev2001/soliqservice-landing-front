import * as React from 'react'
import { useEffect, useState } from 'react'
import { ApiService } from '../../services/api.service'
import footerLogoSvg from '../../assets/img/general/footer-logo.svg'
import googlePlaySvg from '../../assets/img/general/google-play.svg'
import appStoreSvg from '../../assets/img/general/app-store.svg'
import { systemLangs } from '../../assets/systemLangs'
import { systemLocales } from '../../assets/systemLocales'

export const Footer = ({ selectedLang = systemLangs.ru}) => {
  const [footerInfo, setFooterInfo] = useState({
    appStoreLink: null,
    playMarketLink: null
  })
  useEffect(() => {
    ApiService.getComponent('contact-info-and-app-link')
      .then(data => {
        if(!data instanceof Error) setFooterInfo({
          appStoreLink: data.data.attributes.appstore,
          playMarketLink: data.data.attributes.playmarket
        })
      })
  }, [])
  return(
    <footer className="footer">
      <div className="container">
        <div className="footer__wrapper">
          <div className="footer__left">
            <a href="#" className="logo">
              <img src={footerLogoSvg} className="img" alt="" />
            </a>
            <div className="footer__btns">
              <a href={footerInfo.appStoreLink} style={{display: 'block'}} className="footer__btn">
                <img src={appStoreSvg} alt="" className="img" />
              </a>
              <a href={footerInfo.playMarketLink} style={{display: 'block'}} className="footer__btn">
                <img src={googlePlaySvg} alt="" className="img" />
              </a>
            </div>
          </div>
          <div className="footer__center">
            <div className="footer__menu1">
              <a href="/#hero" className="footer__link">{systemLocales.header.taxfree[selectedLang.idx]}</a>
              <a href="/#news" className="footer__link">{systemLocales.header.news[selectedLang.idx]}</a>
              <a href="/#instructions" className="footer__link">{systemLocales.header.instructions[selectedLang.idx]}</a>
            </div>
            <div className="footer__menu2">
              <a href="/#attachments" className="footer__link">{systemLocales.header.download[selectedLang.idx]}</a>
              <a href="/#faq" className="footer__link">{systemLocales.header.faq[selectedLang.idx]}</a>
              <a href="/#contacts" className="footer__link">{systemLocales.header.contacts[selectedLang.idx]}</a>
            </div>
          </div>
          <div className="footer__right">
            <p>
              {systemLocales.footer.legend[selectedLang.idx]}
            </p>
            <div className="footer__copy">
              {systemLocales.footer.copyright[selectedLang.idx]}
            </div>
          </div>
        </div>
      </div>
    </footer>
)}