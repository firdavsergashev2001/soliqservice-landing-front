import * as React from 'react'
import step1png from '../../assets/img/instructions/01.png'
import step2png from '../../assets/img/instructions/02.png'
import step3png from '../../assets/img/instructions/03.png'
import { systemLocales } from '../../assets/systemLocales'
import { systemLangs } from '../../assets/systemLangs'


export const Instructions = ({ selectedLang = systemLangs.ru }) => {
  const { instructions: locales } = systemLocales;
  return (
    <section className="instructions" id="instructions">
      <div className="container">
        <div className="descr-and-title">
          <p className="descr-and-title__desc">{locales.subtitle[selectedLang.idx]}</p>
          <h3 className="descr-and-title__h3">{locales.title[selectedLang.idx]}</h3>
        </div>
        <div className="instructions__wrapper">
          <div className="instructions-card">
            <div className="instructions-card__img">
              <img src={step1png} alt="" className="img" />
            </div>
            <main className="instructions-card__main">
              <h4 className="instructions-card__title">
                {locales.steps[0].title[selectedLang.idx]}
              </h4>
              <ul className="instructions-card__list" 
              dangerouslySetInnerHTML={{ __html: locales.steps[0].content[selectedLang.idx]}}>
              </ul>
            </main>
          </div>
          <div className="instructions-card">
            <div className="instructions-card__img">
              <img src={step2png} alt="" className="img" />
            </div>
            <main className="instructions-card__main">
              <h4 className="instructions-card__title">
                {locales.steps[1].title[selectedLang.idx]}
              </h4>
              <ul className="instructions-card__list" 
              dangerouslySetInnerHTML={{ __html: locales.steps[1].content[selectedLang.idx]}}>
              </ul>
            </main>
          </div>
          <div className="instructions-card">
            <div className="instructions-card__img">
              <img src={step3png} alt="" className="img" />
            </div>
            <main className="instructions-card__main">
              <h4 className="instructions-card__title">
                {locales.steps[2].title[selectedLang.idx]}
              </h4>
              <ul className="instructions-card__list" 
              dangerouslySetInnerHTML={{ __html: locales.steps[2].content[selectedLang.idx]}}>
              </ul>
            </main>
          </div>
        </div>
      </div>
    </section>
)}