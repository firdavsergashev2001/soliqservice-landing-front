import * as React from 'react'
import { useRef, useEffect, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { ApiService } from '../../services/api.service';
import pdfSvg from '../../assets/img/icons/pdf.svg'
import chevronLeft from "../../assets/img/icons/chevron-left.svg"
import { systemLocales } from '../../assets/systemLocales';
import { systemLangs } from '../../assets/systemLangs';

import 'swiper/css';

// const items = [
//   { title: "Буклет о системе Tax Free в Узбекистане"},
//   { title: "Буклет о системе Tax Free в Узбекистане 2"},
//   { title: "Буклет о системе Tax Free в Узбекистане 3"},
//   { title: "Буклет о системе Tax Free в Узбекистане 4"},
//   { title: "Буклет о системе Tax Free в Узбекистане 6"},
//   { title: "Буклет о системе Tax Free в Узбекистане 7"},
// ]

export const Attachments = ({ selectedLang = systemLangs.ru }) => {
  const swiperRef = useRef();
  const { attachments: locale } = systemLocales;
  const [sectionInfo, setSectionInfo] = useState([])
  useEffect(() => {
    ApiService.getComponent(`attachments-section?populate=attachment.file&locale=${selectedLang.localization}`)
      .then(data => {
        setSectionInfo(data.data.attributes.attachment)
      })
  }, [selectedLang])

  return (
    <section className="materials" id="attachments">
       { sectionInfo && sectionInfo.length > 0 && <div className="container">
        <div className="materials__wrapper">
          <div className="materials__left">
            <h3 className="materials__h3">{ locale.title[selectedLang.idx] }</h3>
            <p className="materials__desc">
              { locale.subtitle[selectedLang.idx] }
            </p>
            <a href="#" className="btn btn--accent">{locale.button[selectedLang.idx]}</a>
          </div>
          <div className="materials__right">
            <div className="materials-slider swiper">
              <Swiper className="swiper-wrapper" slidesPerView={1} breakpoints={{
                768: { slidesPerView: 2 },
                992: { slidesPerView: 3 },
                1280: { slidesPerView: 4 }
              }} onBeforeInit={(swiper) => {
                swiperRef.current = swiper;
              }}>
                { sectionInfo && sectionInfo.length > 0 && sectionInfo.map((item, idx) => (
                  <SwiperSlide className="swiper-slide materials-slide" key={idx}>
                    <div className='slide__inner'>
                      <a
                        href={`${process.env.REACT_APP_BACKEND_URL}${item.file.data.attributes.url}`}
                        download
                        className="materials-slide__img"
                      >
                      <img src={pdfSvg} alt="" className="img" />
                      </a>
                      <div className="materials-slide__title">
                        {item.title}
                      </div>
                      </div>
                    </SwiperSlide>
                ))}
                
                
              </Swiper>
              <div className="swiper-button-prev materials-prev prevent-select" onClick={() => {
                    swiperRef.current?.slidePrev()
                }}>
                <img src={chevronLeft} alt="" />
              </div>
              <div className="swiper-button-next materials-next prevent-select" onClick={() => {
                    swiperRef.current?.slideNext()
                }}>
                <img src={chevronLeft} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div> }
    </section>
)}