import * as React from 'react'
import { useState, useEffect } from 'react'
import { ApiService } from '../../services/api.service'
import item2png from '../../assets/img/news/item-2.png'
import item1png from '../../assets/img/news/item-1.png'
import { systemLocales } from '../../assets/systemLocales'
import { systemLangs } from '../../assets/systemLangs'

export const News = ({ selectedLang = systemLangs.ru }) => {
  const { news: newsLocale } = systemLocales;
  const [ sectionData, setSectionData ] = useState([])
  useEffect(() => {
    ApiService.getComponent(`topic?populate=Topics.image&locale=${selectedLang.localization}`)
      .then(data => {
        setSectionData(data.data.attributes.Topics)
      })
  }, [selectedLang])
  return (
    <section className="news-section" id="news">
      <div className="container">
        <div className="heading-and-desc">
          <h3 className="heading-and-desc__h3">{newsLocale.title[selectedLang.idx]}</h3>
          <div className="heading-and-desc__descr">
            {newsLocale.subtitle[selectedLang.idx]}
          </div>
        </div>
        { sectionData[0] && <div className="single-news-item">
          <div className="single-news-item__img">
            <img src={`${process.env.REACT_APP_BACKEND_URL}${sectionData[0].image.data[0].attributes.formats.medium.url}`} alt="" className="img" />
          </div>
          <div className="single-news-item__content">
            <h4 className="single-news-item__title">
              {sectionData[0]?.title}
            </h4>
            <p className="single-news-item__excerpt">
              {sectionData[0]?.description}
            </p>
            <footer className="single-news-item__footer">
              {/* <div className="single-news-item__published">
                15 ноября 2022 г., 15:32
              </div> */}
              <a href={sectionData[0]?.sourceLink || '#'} className="single-news-item__btn">{ newsLocale.readAll[selectedLang.idx] }</a>
            </footer>
          </div>
        </div>}
        {sectionData[1] && <div className="single-news-item right">
          <div className="single-news-item__img">
            <img src={`${process.env.REACT_APP_BACKEND_URL}${sectionData[1].image.data[0].attributes.formats.medium.url}`} alt="" className="img" />
          </div>
          <div className="single-news-item__content">
            <h4 className="single-news-item__title">
              {sectionData[1]?.title}
            </h4>
            <p className="single-news-item__excerpt">
              {sectionData[1]?.description}
            </p>
            <footer className="single-news-item__footer">
              {/* <div className="single-news-item__published">
                15 ноября 2022 г., 15:32
              </div> */}
              <a href={sectionData[1]?.sourceLink || '#'} className="single-news-item__btn">{ newsLocale.readAll[selectedLang.idx] }</a>
            </footer>
          </div>
        </div>}
      </div>
    </section>
)}