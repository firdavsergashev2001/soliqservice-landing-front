import * as React from 'react'
import { useState, useRef, useEffect } from 'react'
import { ApiService } from '../../services/api.service'
import ReCAPTCHA from 'react-google-recaptcha'
import chevronDownSvg from '../../assets/img/icons/chevron-down.svg'
import excludeSvg from '../../assets/img/icons/exclude.svg'
import { NotificationManager} from 'react-notifications';
import { systemLocales } from '../../assets/systemLocales'
import { systemLangs } from '../../assets/systemLangs'

export const Faq = ({ selectedLang = systemLangs.ru, setSelectedLang }) => {
  const { faq: locale } = systemLocales;
  const captchaRef = useRef(null)
  const [active, setActive] = useState(-1)
  const handleSubmit = (e) => {
    e.preventDefault();
    const values = [{
        title: "Имя",
        value: e.target[0].value
      }, 
      {
        title: "Email",
        value: e.target[1].value
      }, 
      {
        title: "Сообщение",
        value: e.target[2].value
      }]
    let error = 0
    values.map((item, key) => {
      if(key === 0 && item.value.length < 4) {
        error++
        return NotificationManager.error(`${systemLocales.notifications.feedback.invalidField[selectedLang.idx]}: "${item.title}"`)
      } else if(key === 1 && item.value.length < 4 && !item.value.includes('@')) {
        error++
        return NotificationManager.error(`${systemLocales.notifications.feedback.invalidField[selectedLang.idx]}: "${item.title}"`)
      } else if(key === 2 && item.value.length < 30) {
        error++
        return NotificationManager.error(`${systemLocales.notifications.feedback.invalidField[selectedLang.idx]}: "${item.title}"`)
      }
    })
    const token = captchaRef.current.getValue();
    if(error !== 0 || !token) return;


    captchaRef.current.reset();
    e.target.reset()
    NotificationManager.success(systemLocales.notifications.feedback.success.message[selectedLang.idx], systemLocales.notifications.feedback.success.title[selectedLang.idx])
  }

  const [sectionInfo, setSectionInfo] = useState([])

  useEffect(() => {
    ApiService.getComponent(`faq-section?populate=%2A&locale=${selectedLang.localization}`)
      .then(data => {
        setSectionInfo(data.data.attributes.questions)
      })
  }, [selectedLang])
  return(
    <section className="faqs" id="faq">
      <div className="container">
        <div className="heading-and-desc">
          <h3 className="heading-and-desc__h3">{ locale.title[selectedLang.idx] }</h3>
          <div className="heading-and-desc__descr">
            { locale.subtitle[selectedLang.idx]}
          </div>
        </div>
        <main className="faqs__main">
          <div className="faq-accordion">
            {sectionInfo && sectionInfo.length > 0 && sectionInfo.map((item, index) => (
              <div key={index} className="faq-accordion-item" onClick={() => active === index ? setActive(-1) : setActive(index)}>
              <header className="faq-accordion-item__header">
                <div className="faq-accordion-item__question">
                  {index+1}. {item.Question}
                </div>
                <div className={active === index ? "faq-accordion-item__icon active" : "faq-accordion-item__icon"}>
                  <img src={chevronDownSvg} width="18" alt="" />
                </div>
              </header>
              <div className="faq-accordion-item__answer" style={active === index ? { height:  'auto', opacity: '1'} : { height:  '0px', padding: '0px', opacity: '0'}}>
                {item.Answer}
              </div>
            </div>
            ))}
          </div>
          <form onSubmit={handleSubmit} className="main-form">
            <h4 className="main-form__h4">{ locale.formTitle[selectedLang.idx] }</h4>
            <p className="main-form__text">
              { locale.formSubtitle[selectedLang.idx]}
            </p>
            <input type="text" placeholder={locale.namePlaceholder[selectedLang.idx]} />
            <input type="email" placeholder={locale.emailPlaceholder[selectedLang.idx]}/>
            <textarea name="message" placeholder={locale.messagePlaceholder[selectedLang.idx]}></textarea>
            <div className="main-form__bottom">
              <div className="agreement">
                  <ReCAPTCHA 
                  sitekey={process.env.REACT_APP_RECAPTCHA_SITE_KEY} //TEMPORARILY process.env.REACT_APP_RECAPTCHA_SITE_KEY || 
                  ref={captchaRef}
                  />
              </div>
              <input type="submit" value={locale.sendButton[selectedLang.idx]} />
            </div>
          </form>
        </main>
        <div className="exclude">
          <img src={excludeSvg} alt="" className="img" />
        </div>
      </div>
    </section>
)}