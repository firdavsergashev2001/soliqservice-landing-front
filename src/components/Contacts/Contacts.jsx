import * as React from 'react'
import { useState, useEffect } from 'react'
import { ApiService } from '../../services/api.service'
import branchsMapSvg from '../../assets/img/general/branchs-map.svg'
import { systemLocales } from '../../assets/systemLocales'
import { systemLangs } from '../../assets/systemLangs'

export const Contacts = ({ selectedLang = systemLangs.ru }) => {
  const { map: locale, branches, contacts} = systemLocales;
  const [sectionInfo, setSectionInfo] = useState({
    phone: null,
    email: null
  })
  useEffect(() => {
    ApiService.getComponent('contact-info-and-app-link')
      .then(data => {
        setSectionInfo({
          phone: data.data.attributes.phone,
          email: data.data.attributes.email
        })
      })
  }, [])
return(
    <section className="contacts-section" id="contacts">
      <div className="container">
        <div className="heading-and-desc">
          <h3 className="heading-and-desc__h3">{locale.title[selectedLang.idx]}</h3>
          <div className="heading-and-desc__descr">
            {locale.subtitle[selectedLang.idx]}
          </div>
        </div>
      </div>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d47979.75284775151!2d69.21107812495653!3d41.27111522144389!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe04bf246835d2d7f!2sIslam%20Karimov%20Tashkent%20International%20Airport!5e0!3m2!1sen!2suk!4v1672060764383!5m2!1sen!2suk"
        width="100%"
        height="682"
        style={{border: 0}}
        allowFullScreen=""
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      ></iframe>
      <div className="container">
        <div className="branchs">
          <h3 className="branchs__h3">
            {branches.subtitle[selectedLang.idx]}
          </h3>
          <div className="branchs__img">
            <img src={branchsMapSvg} alt="" className="img" />
          </div>
        </div>
      </div>
      <div className="address-box">
        <div className="container">
          <div className="address-box__wrapper">
            <div className="address-box__left">
              <h5 className="address-box__h5">{contacts.officeLabel[selectedLang.idx]}: </h5>
              <address className="address-box__add">{contacts.office[selectedLang.idx]}</address>
            </div>
            <div className="address-box__center">
              <h5 className="address-box__h5">{contacts.addressLabel[selectedLang.idx]}: </h5>
              <address className="address-box__add">
                {contacts.address[selectedLang.idx]}
              </address>
            </div>
            <div className="address-box__right">
              <p className="address-box__item">
                {contacts.callCenterLabel[selectedLang.idx]}: &nbsp;
                <a href={`tel:${sectionInfo.phone}`}>{sectionInfo.phone}</a>
              </p>
              <p className="address-box__item">
                E-mail:&nbsp;
                <a href={"mailto:"+ sectionInfo.email }>{sectionInfo.email}</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
)}