import * as React from "react"
import { useState, useEffect } from "react"
import callSvg from '../../assets/img/icons/call.svg'
import loginSvg from '../../assets/img/icons/log-in.svg'
import logoSvg from '../../assets/img/general/logo.svg'
import burgerSvg from '../../assets/img/icons/burger.svg'
import closeSvg from '../../assets/img/icons/close.svg'
import { systemLangs } from "../../assets/systemLangs"
import { systemLocales } from "../../assets/systemLocales"
import { Link } from "gatsby"
import { ApiService } from "../../services/api.service"

export const Header = ({ selectedLang = systemLangs.ru, setSelectedLang }) => { 
  const { header: headerLocales } = systemLocales;
  const [ burgerState, setBurgerState ] = useState(false);
  const menu = [
    {id: '/#about', title: headerLocales.taxfree[selectedLang.idx]},
    {id: '/#news', title: headerLocales.news[selectedLang.idx]},
    {id: '/#instructions', title: headerLocales.instructions[selectedLang.idx]},
    {id: '/#attachments', title: headerLocales.download[selectedLang.idx]},
    {id: '/#faq', title: headerLocales.faq[selectedLang.idx]},
    {id: '/#contacts', title: headerLocales.contacts[selectedLang.idx]},
  ]
  const [headerInfo, setHeaderInfo] = useState({
    phone: null,
    frontOfficeLink: null,
  })
  useEffect(() => {
    ApiService.getComponent('contact-info-and-app-link')
      .then(data => {
        setHeaderInfo({
          phone: data.data?.attributes.phone,
          frontOfficeLink: data.data?.attributes.frontOfficeLink
        })
      })
  }, [])
  return(
    <header className="header">
      <div className="container">
        <div className="header__wrapper">
          <Link href="/" className="logo">
            <img src={logoSvg} className="img" alt="" />
          </Link>
          <div className="burger" onClick={() => {
            setBurgerState(!burgerState) 
            }}>
            <img src={burgerSvg} width="42" alt="" />
          </div>
          <div className={burgerState ? "header__content active" : "header__content"}>
            <div className="close" onClick={() => { setBurgerState(false) }}>
              <img src={closeSvg} width="42" alt="" />
            </div>
            <nav className="header-nav">
                {
                  menu.map((item, index) => (
                    <a href={item.id} key={index} className="header-nav__item">{item.title}</a>
                  ))
                }
            </nav>
            <div className="header-right">
            <div className="lang" onClick={() => {
              if(selectedLang.label == "UZ") { 
                setSelectedLang(systemLangs.ru)
               } else if(selectedLang.label == "РУ") {
                setSelectedLang(systemLangs.en)
               } else if(selectedLang.label == "EN") {
                setSelectedLang(systemLangs.uz)
               }
            }}>
              <div className="lang__item">{selectedLang.label}</div>
            </div>
            <a href={`tel:${headerInfo.phone}`} className="header-right__tel">
              <img src={callSvg} width="21" alt="" />
              {headerInfo.phone}
            </a>
            <a href={headerInfo.frontOfficeLink} className="log-in">
              <img src={loginSvg} width="26" alt="" />
            </a>
          </div>
          </div>
        </div>
      </div>
    </header>
    
    // <header className="header">
    //   <div className="container">
    //     <div className="header__wrapper">
    //       <a href="#" className="logo">
    //         <img src={logoSvg} className="img" alt="" />
    //       </a>
    //       <nav className="header-nav">
    //         {
    //           menu.map((item, index) => (
    //             <a href={item.id} key={index} className="header-nav__item">{item.title}</a>
    //           ))
    //         }
    //       </nav>
          // <div className="header-right">
          //   <div className="lang" onClick={() => {
          //     if(selectedLang.label == "UZ") { 
          //       setSelectedLang(systemLangs.ru)
          //      } else if(selectedLang.label == "РУ") {
          //       setSelectedLang(systemLangs.en)
          //      } else if(selectedLang.label == "EN") {
          //       setSelectedLang(systemLangs.uz)
          //      }
          //   }}>
          //     <div className="lang__item">{selectedLang.label}</div>
          //   </div>
          //   <a href="tel:+998712023282" className="header-right__tel">
          //     <img src={callSvg} width="21" alt="" />
          //     {systemLocales.general.phone}
          //   </a>
          //   <a href="#" className="log-in">
          //     <img src={loginSvg} width="26" alt="" />
          //   </a>
          // </div>
    //     </div>
    //   </div>
    // </header>
)}