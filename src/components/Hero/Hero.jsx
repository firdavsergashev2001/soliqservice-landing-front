import * as React from 'react'
import { useState, useEffect, useRef } from 'react'
import { ApiService } from '../../services/api.service'
import googlePlaySvg from '../../assets/img/general/google-play.svg'
import appStoreSvg from '../../assets/img/general/app-store.svg'
import playSvg from '../../assets/img/icons/play.svg'
import { systemLocales } from '../../assets/systemLocales'
import { systemLangs } from '../../assets/systemLangs'
import previewVideo from '../../assets/preview.mp4'

export const Hero = ({ selectedLang = systemLangs.ru }) => {
  const videoRef = useRef(null)

  const [sectionInfo, setSectionInfo] = useState({
    appStoreLink: null,
    playMarketLink: null
  })
  useEffect(() => {
    ApiService.getComponent('contact-info-and-app-link')
      .then(data => {
        setSectionInfo({
          appStoreLink: data.data.attributes.appstore,
          playMarketLink: data.data.attributes.playmarket
        })
      })
  }, [])
  const { hero: heroLocales } = systemLocales;
  return(
    <section className="hero" id="hero" style={{position: "realtive"}}>
      <video style={{
        position: 'absolute',
        height: '100%',
        width: '100%',
        objectFit: 'cover'
      }} autoplay muted ref={videoRef}>
          <source src={previewVideo} type="video/mp4"/>
        </video>
      <div className="container">
        <div className="hero__wrapper">
          <div className="hero-left">
            <h1 className="hero-left__h1" 
            dangerouslySetInnerHTML={{ __html: `${heroLocales.title[selectedLang.idx]}`}}> 
            </h1>
            <div className="hero-left__btns">
              <a href={sectionInfo.appStoreLink} style={{display: 'block'}} className="hero-left__btn">
                <img src={appStoreSvg} alt="" className="img" />
              </a>
              <a href={sectionInfo.playMarketLink} style={{display: 'block'}} className="hero-left__btn">
                <img src={googlePlaySvg} alt="" className="img" />
              </a>
            </div>
          </div>
          <a
            href={previewVideo}
            data-fancybox="video-gallery"
            className="hero-center"
          >
            <img src={playSvg} alt="" className="img" />
          </a>
          <div className="hero-right">
            <p className="hero-right__bottom">
              {heroLocales.comment[selectedLang.idx]}
            </p>
          </div>
        </div>
      </div>
      <div className="hero__layer"></div>
    </section>
)}