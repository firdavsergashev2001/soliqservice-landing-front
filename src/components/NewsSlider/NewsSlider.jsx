import * as React from 'react';
import { useRef, useEffect, useState } from 'react';
import { ApiService } from '../../services/api.service';
import { Swiper, SwiperSlide } from 'swiper/react';
import news1png from '../../assets/img/news/news-1.png'
import chevronLeftSvg from '../../assets/img/icons/chevron-left.svg'
import { Link } from 'gatsby';

const news = [
  {
    date: '15 ноября 2022 г., 15:32',
    title: 'Систему Tax Free планируют внедрить в Узбекистане 1',
    subtitle: 'Систему возврата суммы налога на добавленную стоимость (TaxFree) для иностранных граждан планируют внедрить в Узбекистане.',
    url: '#'
  },
  {
    date: '15 ноября 2022 г., 15:32',
    title: 'Систему Tax Free планируют внедрить в Узбекистане 2',
    subtitle: 'Систему возврата суммы налога на добавленную стоимость (TaxFree) для иностранных граждан планируют внедрить в Узбекистане.',
    url: '#'
  },
  {
    date: '15 ноября 2022 г., 15:32',
    title: 'Систему Tax Free планируют внедрить в Узбекистане 3',
    subtitle: 'Систему возврата суммы налога на добавленную стоимость (TaxFree) для иностранных граждан планируют внедрить в Узбекистане.',
    url: '#'
  },
  {
    date: '15 ноября 2022 г., 15:32',
    title: 'Систему Tax Free планируют внедрить в Узбекистане 4',
    subtitle: 'Систему возврата суммы налога на добавленную стоимость (TaxFree) для иностранных граждан планируют внедрить в Узбекистане.',
    url: '#'
  },
  {
    date: '15 ноября 2022 г., 15:32',
    title: 'Систему Tax Free планируют внедрить в Узбекистане 5',
    subtitle: 'Систему возврата суммы налога на добавленную стоимость (TaxFree) для иностранных граждан планируют внедрить в Узбекистане.',
    url: '#'
  },
  {
    date: '15 ноября 2022 г., 15:32',
    title: 'Систему Tax Free планируют внедрить в Узбекистане 6',
    subtitle: 'Систему возврата суммы налога на добавленную стоимость (TaxFree) для иностранных граждан планируют внедрить в Узбекистане.',
    url: '#'
  }
]

export const NewsSlider = ({selectedLang}) => {
  const [ sectionState, setSectionState ] = useState([])
  const swiperRef = useRef();
  useEffect(() => {
    ApiService.getComponent(`news?sort[0]=publishedAt:desc&populate=%2A&locale=${selectedLang.localization}`)
      .then((data) => {
        setSectionState(data.data)
      })
  }, [selectedLang])
  return(
    <section className="news-slider-section" id="newsSlider">
      { sectionState && sectionState.length > 0 && <div className="container">
        <div className="news-swiper swiper">
          <Swiper className="swiper-wrapper" breakpoints={{
            992: {
              slidesPerView: 3
            },
            768: {
              slidesPerView: 2
            },
            320: {
              slidesPerView: 1
            }
          }} slidesPerView={1} onBeforeInit={(swiper) => {
                swiperRef.current = swiper;
              }}>
            { sectionState && sectionState.length > 0 && sectionState.map((item, key) => (
              <SwiperSlide key={key} style={{padding: '10px'}}>
                <div className="swiper-slide news-slide">
                  <div className="news-slide__img">
                    <img src={`${process.env.REACT_APP_BACKEND_URL}${item.attributes.image?.data.attributes.formats.small.url}`} alt="" className="img" />
                  </div>
                  <main className="news-slide__main">
                    <div className="news-slide__date">{item.date}</div>
                    <Link to={`/news/${item.id}?lang=${selectedLang.localization}`} className="news-slide__title">
                      {item.attributes.heading}
                    </Link>
                    <p className="news-slide__excerpt">
                      {item.attributes.text.substr(0, 120)}...
                    </p>
                </main>
                </div>
              </SwiperSlide>
            ))}
            
          </Swiper>

          <div className="swiper-button-prev news-prev disabled prevent-select" onClick={() => {
                    swiperRef.current?.slidePrev()
                }}>
            <img src={chevronLeftSvg} alt="" />
          </div>
          <div className="swiper-button-next news-next prevent-select" onClick={() => {
                    swiperRef.current?.slideNext()
                }}>
            <img src={chevronLeftSvg} alt="" />
          </div>
        </div>
        {/* <div className="news-section__bottom">
          <a href="#" className="news-section__btn btn">Перейти ко всем новостям</a>
        </div> */}
      </div>}
    </section>
)}