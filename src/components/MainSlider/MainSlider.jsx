import * as React from 'react'
import { systemLangs } from '../../assets/systemLangs'
import { systemLocales } from '../../assets/systemLocales'
import excludeSvg from '../../assets/img/icons/exclude.svg'
import slide1png from '../../assets/img/general/slide-1.png'
import item2png from '../../assets/img/news/item-2.png'
import item1png from '../../assets/img/news/item-1.png'


export const MainSlider = ({ selectedLang = systemLangs.ru }) => {
  const { about: aboutLocales } = systemLocales;
  return(
    <section className="text-img-slider" id="about">
      <div className="container">
        <div className="text-img-slider__wrapper">
          <div className="text-img-slider__left">
            <div className="text-img-swiper swiper">
              <div className="swiper-wrapper">
                <div className="swiper-slide text-img-slide">
                  <h5>{aboutLocales.title[selectedLang.idx]}</h5>
                  <h3>{aboutLocales.subtitle[selectedLang.idx]}</h3>
                  <p>
                    {aboutLocales.describtion[selectedLang.idx]}
                  </p>
                  <p dangerouslySetInnerHTML={{ __html: aboutLocales.additional[selectedLang.idx]}}>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="text-img-slider__right">
            <div className="img-text-swiper swiper">
              <div className="swiper-wrapper">
                <div className="swiper-slide img-text-slide">
                  <img src={slide1png} className="img" alt="" />
                </div>
                <div className="swiper-slide img-text-slide">
                  <img src={item2png} className="img" alt="" />
                </div>
                <div className="swiper-slide img-text-slide">
                  <img src={item1png} className="img" alt="" />
                </div>
              </div>
              <div className="swiper-scrollbar img-text-scrollbar"></div>
            </div>
          </div>
        </div>
        <div className="exclude">
          <img src={excludeSvg} alt="" className="img" />
        </div>
      </div>
    </section>
)}