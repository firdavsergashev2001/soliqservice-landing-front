import * as React from 'react'
import { Link } from 'gatsby';
// import shareSvg from '../../assets/img/icons/share.svg'
// import downloadSvg from '../../assets/img/icons/download.svg'
// import newsImg from '../../assets/img/news/news-1.png'
//import eyeSvg from '../../assets/img/icons/eye.svg'

export const Article = ({articleData, topics}) => {
  const { attributes: article } = articleData;
    return(
    <>
    <section className="news-wrapper">
      <div className="container">
        <h3 className="news-main__h3">
          {article?.heading}
        </h3>
        <div className="news-wrapper__content">
          <main className="news-main">
            <div className="news-main__img">
              { article?.image && <img src={process.env.REACT_APP_BACKEND_URL + article?.image.data.attributes.url} alt="" className="img" /> }
            </div>
            <p>
              {article?.text}
            </p>

            <footer className="news-main__footer">
              <div className="news-main__published">{article?.publishedAt.split('T')[0]}</div>
              <div className="news-main__bottom">
                {/* <div className="news-main__views">
                  <img src={eyeSvg} width="35" alt="" />
                  15254
                </div> */}
                {/* <a
                  href="./img/icons/download.svg"
                  download
                  className="news-main__download"
                >
                  <img src={downloadSvg} width="35" alt="" />
                  Скачать
                </a>
                <a href="#" className="news-main__share">
                  <img src={shareSvg} width="35" alt="" />
                  Поделиться
                </a> */}
              </div>
            </footer>
          </main>
          <aside className="news-aside">
            {topics && topics.map((item) => (
              <Link key={item.id} href={`/news/${item.id}`} className="news-card">
              <div className="news-card__img">
                <img src={process.env.REACT_APP_BACKEND_URL + item.attributes.image.data.attributes.formats.small.url} alt="" className="img" />
              </div>
              <main className="news-card__main">
                <div className="news-card__top">
                  <div className="news-card__published">
                  {item.attributes.publishedAt.split('T')[0]}
                  </div>
                  {/* <div className="news-card__views">
                    <img src="./img/icons/eye.svg" width="35" alt="" />
                    15254
                  </div> */}
                </div>
                <h4 className="news-card__title">
                  {item.attributes.heading}
                </h4>
                <p className="news-card__exceprt">
                  {item.attributes.text.substring(0, 80)} ...
                </p>
              </main>
            </Link>
            ))}
          </aside>
        </div>
      </div>
    </section>
    </>
    )
}