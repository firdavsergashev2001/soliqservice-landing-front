import * as React from "react"
import { useState } from "react"

import "../assets/css/aos.css"
import "../assets/css/custom.css"
import "../assets/css/fancybox.css"
import "../assets/css/selectric.css"
import "../assets/css/swiper-bundle.min.css"

import { Header } from "../components/Header/Header"
import { Footer } from "../components/Footer/Footer"
import { NotificationContainer } from "react-notifications"
import "../assets/sass/main.scss"
import { systemLangs } from "../assets/systemLangs"
import { Article } from "../components/Article/Article"

const ArticlePage = () => {
  const [selectedLang, setSelectedLang] = useState(systemLangs.ru)
  return (
    <>
      <NotificationContainer/>
      <Header selectedLang={selectedLang} setSelectedLang={setSelectedLang}/>
      <Article/>
      <Footer selectedLang={selectedLang}/>
  </>
  )
}

export default ArticlePage

export const Head = () => <title>Tax Free - система возврата иностранным туристам НДС за товары</title>
