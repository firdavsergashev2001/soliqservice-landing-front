import * as React from "react"
import { useState } from "react"
import { Link } from "gatsby"
import { Header } from "../components/Header/Header"
import { Footer } from "../components/Footer/Footer"
import { systemLangs } from "../assets/systemLangs"
import { systemLocales } from "../assets/systemLocales"

const pageStyles = {
  color: "#232129",
  padding: "96px",
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
}

const NotFoundPage = () => {
  const [selectedLang, setSelectedLang] = useState(systemLangs.ru)
  return (
    <>
      <Header selectedLang={selectedLang} setSelectedLang={setSelectedLang}/>
      <main style={pageStyles}>
        <h1 style={headingStyles}>{systemLocales[404].title[selectedLang.idx]}</h1>
        <Link to="/">{systemLocales[404].link[selectedLang.idx]}</Link>
      </main>
      <Footer selectedLang={selectedLang}/>
    </>
    
  )
}

export default NotFoundPage

export const Head = () => <title>Not found</title>
