import * as React from "react"
import { useState } from "react"

import "../assets/css/aos.css"
import "../assets/css/custom.css"
import "../assets/css/fancybox.css"
import "../assets/css/selectric.css"
import "../assets/css/swiper-bundle.min.css"

import { Header } from "../components/Header/Header"
import { Hero } from "../components/Hero/Hero"
import { MainSlider } from "../components/MainSlider/MainSlider"
import { News } from "../components/News/News"
import { NewsSlider } from "../components/NewsSlider/NewsSlider"
import { Instructions } from "../components/Instructions/Instructions"
import { Attachments } from "../components/Attachments/Attachments"
import { Faq } from "../components/Faq/Faq"
import { Contacts } from "../components/Contacts/Contacts"
import { Footer } from "../components/Footer/Footer"
import { NotificationContainer } from "react-notifications"
import "../assets/sass/main.scss"
import { systemLangs } from "../assets/systemLangs"

const IndexPage = () => {
  const [selectedLang, setSelectedLang] = useState(systemLangs.ru)
  return (
    <>
      <NotificationContainer/>
      <Header selectedLang={selectedLang} setSelectedLang={setSelectedLang}/>
      <Hero selectedLang={selectedLang}/>
      <MainSlider selectedLang={selectedLang}/>
      <News selectedLang={selectedLang}/>
      <NewsSlider selectedLang={selectedLang}/>
      <Instructions selectedLang={selectedLang}/>
      <Attachments selectedLang={selectedLang}/>
      <Faq selectedLang={selectedLang}/>
      <Contacts selectedLang={selectedLang}/>
      <Footer selectedLang={selectedLang}/>

      {/* <Script src="https://code.jquery.com/jquery-3.6.3.min.js"/>
      <Script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"/>
      <Script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"/>
      <Script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"/>
      <Script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/8.4.5/swiper-bundle.min.js"/>
      <script src="./main.js"/> */}
  </>
  )
}

export default IndexPage

export const Head = () => <title>Tax Free - система возврата иностранным туристам НДС за товары</title>
