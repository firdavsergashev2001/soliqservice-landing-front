import * as React from "react"
import { useState, useEffect } from "react"

import "../../assets/css/aos.css"
import "../../assets/css/custom.css"
import "../../assets/css/fancybox.css"
import "../../assets/css/selectric.css"
import "../../assets/css/swiper-bundle.min.css"

import { Header } from "../../components/Header/Header"
import { Footer } from "../../components/Footer/Footer"
import { NotificationContainer } from "react-notifications"
import "../../assets/sass/main.scss"
import { systemLangs } from "../../assets/systemLangs"
import { Article } from "../../components/Article/Article"
import { ApiService } from "../../services/api.service"

const ArticlePage = ({location}) => {
  const [ articleId, setArticleId ] = useState(location.pathname.split('/')[2]);
  const lang = location.search.split('=')[1]
  const [articleData, setArticleData] = useState({})
  const [selectedLang, setSelectedLang] = useState(systemLangs.ru)
  const [topics, setTopics] = useState([])

  useEffect(() => {
    ApiService.getArticle(articleId, selectedLang.localization)
    .then((data) => {
        setArticleData(data.data)
    })
    setSelectedLang(systemLangs[lang])
  }, [])
  useEffect(() => {
    const articleLocalizations = articleData.attributes?.localizations.data;
    const target = articleLocalizations?.find(({attributes}) => attributes.locale === selectedLang.localization)
    target && ApiService.getArticle(target.id, selectedLang.localization)
    .then((data) => {
        setArticleData(data.data)
    })

    ApiService.getComponent(`news?sort[0]=publishedAt:desc&populate=%2A&locale=${selectedLang.localization}`)
    .then((data) => {
        setTopics(data.data)
    })
  }, [selectedLang, articleId]);
  return (
    <>
      <NotificationContainer/>
      <Header selectedLang={selectedLang} setSelectedLang={setSelectedLang}/>
      <Article articleData={articleData || { attributes: null}} topics={topics}/>
      <Footer selectedLang={selectedLang}/>
  </>
  )
}

export default ArticlePage

export const Head = () => <title>Tax Free - система возврата иностранным туристам НДС за товары</title>
